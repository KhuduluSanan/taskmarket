package az.relationship.market.service.impl;

import az.relationship.market.config.AppModelMapper;
import az.relationship.market.dto.AddressDto;
import az.relationship.market.model.Address;
import az.relationship.market.repo.AddressRepository;
import az.relationship.market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddressServiceImpl implements AddressService {
    private final AppModelMapper appModelMapper;
    private final AddressRepository addressRepository;

    @Override
    public Long creatAddress(AddressDto addressDto) {
        Address address = appModelMapper.getMapper().map(addressDto, Address.class);
        return addressRepository.save(address).getId();
    }

}

