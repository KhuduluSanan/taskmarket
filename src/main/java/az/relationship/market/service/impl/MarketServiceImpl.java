package az.relationship.market.service.impl;

import az.relationship.market.config.AppModelMapper;
import az.relationship.market.dto.MarketDto;
import az.relationship.market.model.Market;
import az.relationship.market.repo.MarketRepository;
import az.relationship.market.service.MarketService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MarketServiceImpl implements MarketService {

    private final AppModelMapper appModelMapper;
    private final MarketRepository marketRepository;

    @Override
    public Long saveMarket(MarketDto marketDto) {
        Market map = appModelMapper.getMapper().map(marketDto, Market.class);
        return marketRepository.save(map).getId();
    }
}

