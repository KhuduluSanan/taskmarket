package az.relationship.market.service.impl;

import az.relationship.market.config.AppModelMapper;
import az.relationship.market.dto.BranchDto;
import az.relationship.market.exception.NotFoundException;
import az.relationship.market.model.Branch;
import az.relationship.market.repo.BranchRepository;
import az.relationship.market.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.crossstore.ChangeSetPersister;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BranchServiceImpl implements BranchService {

    private final AppModelMapper appModelMapper;
    private final BranchRepository branchRepository;

    @Override
    public Long createBranch(BranchDto branchDto) {
        Branch mapped = appModelMapper.getMapper().map(branchDto, Branch.class);
        branchRepository.save(mapped);
        return branchDto.getMarketId();
    }

    @Override
    public List<BranchDto> getAllBranches() {
        List<Branch> branches = branchRepository.findAll();
        return branches.stream()
                .map(branch -> appModelMapper.getMapper().map(branch, BranchDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<BranchDto> getID() {
        List<Branch> branches = branchRepository.findAll();
        List<BranchDto> branchDTOs = new ArrayList<>();
        for (Branch branch : branches) {
            BranchDto branchDto = appModelMapper.getMapper().map(branch, BranchDto.class);
            branchDto.setMarketName(branch.getMarket().getName());
            branchDto.setAddressName(branchDto.getAddressName());
            branchDTOs.add(branchDto);
        }
        return branchDTOs;
    }
}



