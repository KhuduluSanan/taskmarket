package az.relationship.market.service;

import az.relationship.market.dto.MarketDto;

public interface MarketService {
    Long saveMarket(MarketDto marketDto);
}
