package az.relationship.market.service;

import az.relationship.market.dto.BranchDto;
import az.relationship.market.model.Branch;

import java.util.List;

public interface BranchService {

    Long createBranch(BranchDto branchDto);

    List<BranchDto> getAllBranches();

    List<BranchDto> getID();
}
