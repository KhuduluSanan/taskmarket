package az.relationship.market.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppModelMapper {

    @Bean
    public ModelMapper getMapper() {
        return new ModelMapper();
    }
}
