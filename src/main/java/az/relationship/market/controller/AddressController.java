package az.relationship.market.controller;

import az.relationship.market.dto.AddressDto;
import az.relationship.market.service.AddressService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/address")
public class AddressController {
    private final AddressService addressService;

    @PostMapping
    public Long creatAddress(@RequestBody AddressDto addressDto){
        return addressService.creatAddress(addressDto);
    }
}
