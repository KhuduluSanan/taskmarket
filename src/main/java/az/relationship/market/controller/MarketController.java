package az.relationship.market.controller;

import az.relationship.market.dto.MarketDto;
import az.relationship.market.model.Market;
import az.relationship.market.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/market")
public class MarketController {
    private final MarketService marketService;

    @PostMapping
    public Long saveMarket(@Valid @RequestBody MarketDto marketDto){
        return marketService.saveMarket(marketDto);
    }
}
