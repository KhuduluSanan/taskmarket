package az.relationship.market.controller;

import az.relationship.market.dto.BranchDto;
import az.relationship.market.dto.MarketDto;
import az.relationship.market.model.Address;
import az.relationship.market.model.Branch;
import az.relationship.market.service.BranchService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/branch")
public class BranchController {
    private final BranchService branchService;


    @PostMapping
    public Long createBranch(@Valid @RequestBody BranchDto branchDto) {
        return branchService.createBranch(branchDto);
    }


    @GetMapping("/branches")
    public ResponseEntity<List<BranchDto>> getAllBranches() {
        List<BranchDto> branchDTOs = branchService.getAllBranches();
        return ResponseEntity.ok(branchDTOs);
    }

    @GetMapping("/{id}")
    public List<BranchDto> getId(@PathVariable Long id){
         List<BranchDto> branchDtos = branchService.getID();
        return branchDtos;
    }


}
